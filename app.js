module.exports = app => {
    app.beforeStart(async () => {
        const menu = {
            button: [
                {
                    name: '菜单',
                    sub_button: [
                        {
                            type: 'click',
                            name: '点击',
                            key: 'click',
                        },
                        {
                            type: 'view',
                            name: '博客',
                            url: 'https://www.rmrfjune.cn',
                        },
                    ],
                },
                {
                    name: '菜单',
                    sub_button: [
                        {
                            type: 'scancode_waitmsg',
                            name: '扫码带提示',
                            key: 'scancode_waitmsg',
                        },
                        {
                            type: 'scancode_push',
                            name: '扫码推事件',
                            key: 'scancode_push',
                        },
                        {
                            type: 'pic_sysphoto',
                            name: '系统拍照发图',
                            key: 'pic_sysphoto',
                        },
                        {
                            type: 'pic_photo_or_album',
                            name: '拍照或者相册发图',
                            key: 'pic_photo_or_album',
                        },
                        {
                            type: 'pic_weixin',
                            name: '微信相册发图',
                            key: 'pic_weixin',
                        },
                    ],
                },
                {
                    type: 'location_select',
                    name: '发送位置',
                    key: 'location_select',
                },
            ],
        };

        // 开始没有考虑到菜单这个，获取accessToken这里冗余了
        let access_token
        access_token = await app.redis.get('accessToken')
        if (!access_token) {
            const getAccessToken = await app.curl(`https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${app.config.wechat.appId}&secret=${app.config.wechat.appSecret}`, {
                method: 'GET',
                dataType: 'json'
            })
            access_token = getAccessToken.data.access_token
        }
        const res = await app.curl(`https://api.weixin.qq.com/cgi-bin/menu/create?access_token=${access_token}`, {
            method: 'POST',
            contentType: 'json',
            data: menu,
            dataType: 'json',
        });

    });
};