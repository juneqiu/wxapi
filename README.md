### Development
```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy
```bash
$ npm start
$ npm stop
```

### npm scripts
- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.

#### 注意
<font size='12' color='#f34250'>代码为测试用，未整理，仅供参考</font>
`公众号我用了花生壳做代理，还有redis缓存`
`代码不提供账号，所有账号请在config目录下的accounts.js`


#### app controller
+ wechat // 公众号相关
+ wxFace // 腾讯云人脸识别 ocr
+ wxMini // 微信相关（支付，openid等)