'use strict';

/** @type Egg.EggPlugin */
module.exports = {
    mongoose: {
        enable: true,
        package: 'egg-mongoose'
    },
    redis: {
        enable: true,
        package: 'egg-redis',
    },
    nunjucks: {
        enable: true,
        package: 'egg-view-nunjucks',
    }
};
