/* eslint valid-jsdoc: "off" */

'use strict';
const accounts = require('./accounts.js')
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
    const config = exports = {};
    config.keys = appInfo.name + '_1593079315190_7206';

    config.security = {
        csrf: {
            enable: false
        },
        domainWhiteList: ['*']
    };

    config.wechat = { ...accounts.wechat }; // 公众号
    config.wechatMini = { ...accounts.wechatMini }; //小程序
    config.wetchatApp = { ...accounts.wetchatApp }; // app
    config.wechatPay = { ...accounts.wechatPay }; // 商户号

    config.middleware = ['errorHandler', 'compress'];

    config.compress = {
        threshold: 2048,
    };

    config.multipart = {
        mode: 'file',
        fieldSize: 5242880
    };

    // oss
    config.oss = {
        client: {
            timeout: '60s',
            ...accounts.ali_oss
        }
    };

    // config.mongoose = { // mongoose
    //     url: process.env.EGG_MONGODB_URL || 'mongodb://127.0.0.1/wechat',
    //     options: {
    //         server: {
    //             poolSize: 40,
    //         },
    //     }, 
    // };

    config.view = {
        defaultViewEngine: 'nunjucks',
        defaultExtension: '.nj',
    };

    config.redis = {
        client: {
            port: 6379,          // Redis port
            host: '127.0.0.1',   // Redis host
            password: '123456',
            db: 0,
        },
    };


    // add your user config here
    const userConfig = {
        // myAppName: 'egg',
    };

    return {
        ...config,
        ...userConfig,
    };
};
