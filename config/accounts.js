module.exports = {
    wechat: { // 公众号
        appId: '',
        appSecret: '',
        token: '',
        baseUrl: ''
    },
    wechatMini: { // 小程序
        appId: '',
        appSecret: ''
    },
    wetchatApp: { // app
        appId: '',
        appSecret: ''
    },
    wechatPay: { // 商户号
        MCHID: '',
        key: '' // 商户号api密钥
    },
    appPush: { // app推送
        appId: '',
        appKey: '',
        appSecret: '',
        masterSecret: ''
    },

    tx: { // 腾讯云
        appId: '',
        secretId: '',
        secretKey: ''

    },

    tx_cos: { // 腾讯cos
        bucket: '',
        region: ''
    },

    ali_oss: {
        accessKeyId: '',
        accessKeySecret: '',
        bucket: '',
        endpoint: ''
    }
}
