'use strict';

/**
 * formatPushMsg
 * @desc 格式化推送消息参数 http://docs.getui.com/getui/server/rest_v2/push/
 * @param { Object } option
 * options.type 推送类型 notification：通知消息内容  transmission：纯透传消息内容 只能选一个，不然报错
 * options.notification = {
 *      title: '请填写你的通知标题', *
 *      body: '请填写你的通知内容', *
 *      big_text: '长文本消息内容，通知消息+长文本样式，与big_image二选一，两个都填写时报错',
 *      big_image: 'http',
 *      logo: 'logo.png', // 必须带后缀名
 *      logo_url: 'http',
 *      channel_level: 3,
 *      click_type: 'payload',
 *      payload: {test:333}
 * }
 * options.push_channel = { // 太多了 查看文档吧
 *      ios: {},
 *      android: {}
 * }
 */
const formatPushMsg = options => {
    const formatObj = {
        request_id: "june" + Date.now() + "push", // 请求唯一标识号，10-32位之间；如果request_id重复，会导致消息丢失
        audience: options.audience,
        settings: { // 推送条件设置
            // ttl: 10 * 1000, // 消息离线时间设置
            // strategy: 
        },
        push_message: {}, // 个推推送消息参数
        push_channel: { // 厂商推送消息参数
            ios: { ...options.push_channel.ios },
            android: { ...options.push_channel.android }
        }
    }
    switch (options.type) {
        case 'notification': // 通知消息内容
            const start_show = Date.now();
            const end_show = start_show + 86400000;
            formatObj.push_message = {
                duration: `${start_show}-${end_show}`, // 手机端通知展示时间段 可不填
                notification: {
                    ...options.notification
                }
            }
            break;
        case 'transmission': // 纯透传消息内容
            formatObj.push_message = {
                duration: `${start_show}-${end_show}`, // 手机端通知展示时间段 可不填
                transmission
            }
            break;
        default:
            break;
    }
    return formatObj
};

module.exports = {
    formatPushMsg
};
