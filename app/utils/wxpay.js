const crypto = require('crypto')
/**
 * rnd32
 * @desc 32位随机字符串
 */
function rnd32() {
    const str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let psd = ''
    for (let i = 0; i < 32; i++) {
        let idx = Math.floor(Math.random() * str.length)
        psd += str[idx]
    }
    return psd
}

/**
 * timeStamp
 * @desc 时间戳
 */
function timeStamp() {
    return Math.round(new Date().getTime() / 1000)
}

/**
 * tradeNo
 * @desc 商户订单号
 */
function tradeNo() {
    const date = new Date()
    const arr = [
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds(),
        Math.floor(Math.random() * 100000)
    ]
    return arr.join('')
}

/**
 * ipAdress
 * @desc 获取ip
 */
function ipAdress() {
    let interfaces = require('os').networkInterfaces();
    for (var devName in interfaces) {
        var iface = interfaces[devName];
        for (var i = 0; i < iface.length; i++) {
            let alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                return alias.address
            }
        }
    }
}

/**
 * createSign
 * @desc 生成签名
 */
function createSign(payParam, mch_key) {
    let arr = []
    for (const key in payParam) {
        arr.push(`${key}=${payParam[key]}`)
    }
    arr = arr.sort((a, b) => a.localeCompare(b))
    arr.push(`key=${mch_key}`)
    arr = arr.join('&')
    return crypto.createHash('md5').update(arr, 'utf-8').digest('hex')
}

module.exports = {
    rnd32,
    timeStamp,
    tradeNo,
    ipAdress,
    createSign
}