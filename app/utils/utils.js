// 部分函数在wxpay和wxface中重复， 懒得改之前的代码
const crypto = require('crypto')
/**
 * ipAdress
 * @desc 获取ip
 */
function ipAdress() {
    let interfaces = require('os').networkInterfaces();
    for (var devName in interfaces) {
        var iface = interfaces[devName];
        for (var i = 0; i < iface.length; i++) {
            let alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                return alias.address
            }
        }
    }
}

/**
 * timeStamp
 * @desc 时间戳
 */
function timeStamp() {
    return Math.round(new Date().getTime() / 1000)
}

/**
 * rnd32
 * @desc 32位随机字符串
 */
function rnd32() {
    const str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let psd = ''
    for (let i = 0; i < 32; i++) {
        let idx = Math.floor(Math.random() * str.length)
        psd += str[idx]
    }
    return psd
}

function raw(args) {
    var keys = Object.keys(args);
    keys = keys.sort()
    var newArgs = {};
    keys.forEach(function (key) {
        newArgs[key.toLowerCase()] = args[key];
    });

    var string = '';
    for (var k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
    }
    string = string.substr(1);
    return string;
}

/**
 * signature
 * @desc 公众号sha1 字符串加密
 * @param { String } url
 */
function createSignature(jsapi_ticket, url) {
    const nonceStr = rnd32()
    const timestamp = timeStamp()
    let obj = {
        jsapi_ticket,
        nonceStr,
        timestamp,
        url
    };
    const string = raw(obj)
    const signature = crypto.createHash('sha1').update(string).digest('hex')
    return {
        signature, nonceStr, timestamp
    }
}

/**
 * nonce
 * @desc 签名的随机串
 */
function nonceStr() {
    return Math.random().toString(36).substr(2, 15);
}

module.exports = {
    ipAdress,
    timeStamp,
    rnd32,
    nonceStr,
    createSignature
}