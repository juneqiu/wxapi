const tencentcloud = require("tencentcloud-sdk-nodejs")
// 导入对应产品模块的client models。
const CvmClient = tencentcloud.cvm.v20170312.Client;
const models = tencentcloud.cvm.v20170312.Models;
const Credential = tencentcloud.common.Credential;
const crypto = require('crypto')
function detectAuth() {
    // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
    let cred = new Credential("AKID1AAtMZAMnxtbQ0BMhkWrlU1c99sKDovl", "iaTklVWXtzp1AdzBqEdbIVliE3ey1nMr");

    // 实例化要请求产品(以cvm为例)的client对象
    let client = new CvmClient(cred, "ap-guangzhou");

    // 实例化一个请求对象
    let req = new models.DescribeZonesRequest();
    // 通过client对象调用想要访问的接口，需要传入请求对象以及响应回调函数
    return new Promise((resolve, reject) => {
        client.DescribeZones(req, function (err, response) {
            // 请求异常返回，打印异常信息
            if (err) {
                console.log(err);
                reject(err);
            }
            // 请求正常返回，打印response对象
            resolve(response.to_json_string())
        });
    })
}

/**
 * createSign
 * @desc 生成签名
 */
function createSign(params) {
    console.log(params)
    let arr = []
    for (const key in params) {
        arr.push(`${params[key]}`)
    }
    arr = arr.sort((a, b) => a.localeCompare(b))
    let str = ''
    for (let i = 0; i < arr.length; i++) {
        str += arr[i]
    }
    return crypto.createHash('sha1').update(str, 'utf-8').digest('hex')
}

function tradeNo() {
    const date = new Date()
    const arr = [
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds(),
        Math.floor(Math.random() * 100000)
    ]
    return arr.join('')
}

/**
 * rnd32
 * @desc 32位随机字符串
 */
function rnd32() {
    const str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let psd = ''
    for (let i = 0; i < 32; i++) {
        let idx = Math.floor(Math.random() * str.length)
        psd += str[idx]
    }
    return psd
}

module.exports = {
    detectAuth,
    createSign,
    tradeNo,
    rnd32,
    createSign
}