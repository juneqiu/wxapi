'use strict';
const Service = require('egg').Service;
const { createXml } = require('../utils/xml2js');

class WechatMsg extends Service {
    /**
     * handleEvent
     * @desc 事件类型
     * @param { Object } message 
     */
    async handleEvent(message) {
        const { FromUserName, Event, EventKey, Ticket, Latitude, Longitude, Precision } = message;
        let reply;
        switch (Event) {
            case 'subscribe': // 关注事件
                reply = '欢迎关注我的测试公众号';
                break;
            case 'unsubscribe': // 取消关注事件
                reply = '';
                break;
            case 'SCAN': // 扫码
                reply = 'EventKey:' + EventKey + ', Ticket:' + Ticket;
                break;
            case 'LOCATION': // 位置
                reply = 'Latitude:' + Latitude + ', Longitude:' + Longitude + ', Precision:' + Precision;
                break;
            case 'CLICK': // 点击
                reply = 'EventKey:' + EventKey;
                break;
            case 'VIEW': // 点击菜单跳转链接时的事件推送
                reply = 'EventKey:' + EventKey;
                break;
            default:
                reply = '';
                break;
        }
        return reply;
    }

    /**
     * handleMsg
     * @desc 消息
     * @param { Object } message 
     */
    async handleMsg(message) {
        const { MsgType, Content, PicUrl, MediaId, Recognition, Label, Url } = message;
        let reply;
        switch (MsgType) {
            case 'text': // 文本
                reply = Content;
                break;
            case 'image': // 图片
                reply = PicUrl;
                break;
            case 'voice': // 语音
                console.log(Recognition);
                reply = MediaId;
                break;
            case 'video': // 视频
                reply = MediaId;
                break;
            case 'shortvideo': // 短视频
                reply = MediaId;
                break;
            case 'location': // 位置
                reply = Label;
                break;
            case 'link': // 链接
                reply = Url;
                break;
            default:
                reply = '';
                break;
        }
        return reply;
    }

    /**
     * replyMsg
     * @desc 回复信息转xml
     * @param {*} message 
     * @param {*} Content 
     */
    async replyMsg(message, Content) {
        const obj = {
            ToUserName: message.FromUserName,
            FromUserName: message.ToUserName,
            CreateTime: new Date().getTime(),
            MsgType: 'text',
            Content,
        };
        return createXml(obj);
    }

}

module.exports = WechatMsg;
