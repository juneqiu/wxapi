'use strict';

const Service = require('egg').Service;

const crypto = require('crypto')

class WxDecrypt extends Service {

    // 解密加密信息
    async decrypt(sessionKeyData, encryptedData, ivData) {
        try {
            const { ctx, app } = this;
            const sessionKey = Buffer.from(sessionKeyData, 'base64')
            const encrypt = Buffer.from(encryptedData, 'base64')
            const iv = Buffer.from(ivData, 'base64')
            const { appId } = app.config.wechatMini

            // 解密
            let decipher = crypto.createDecipheriv('aes-128-cbc', sessionKey, iv)
            // 设置自动 padding 为 true，删除填充补位
            decipher.setAutoPadding(true)
            let decoded = decipher.update(encrypt, 'binary', 'utf8')
            decoded += decipher.final('utf8')
            decoded = JSON.parse(decoded)
            if (decoded.watermark.appid !== appId) {
                throw new Error('Illegal Buffer')
            }
            return decoded
        } catch (error) {
            console.log(error)
            return false
        }

    }



}

module.exports = WxDecrypt;
