'use strict';

const Service = require('egg').Service;

const crypto = require('crypto');
const baseUrl = 'https://restapi.getui.com/v2/';
const accounts = require('../../config/accounts.js');

class GeTui extends Service {
    /**
    * getToken
    * @desc 获取个推token
    */
    async getGeTuiToken() {
        const { ctx } = this;
        const { appId, appKey, appSecret, masterSecret } = accounts.appPush;
        const timestamp = Date.now();
        const sign = crypto.createHash('sha256').update(`${appKey}${timestamp}${masterSecret}`).digest('hex');
        const res = await ctx.curl(`${baseUrl}${appId}/auth`, {
            method: 'POST',
            contentType: 'json',
            data: {
                sign,
                timestamp,
                appkey: appKey
            },
            dataType: 'json'
        });
        return res.data.data
    }

    /**
     * pushMsg
     * @desc 推送消息
     */
    async pushMsg(options) {
        const { ctx, service } = this;
        let token = await service.cache.get('getui_token');
        if (!token) {
            token = await service.push.getGeTuiToken();
        }
        const data = {
            type: 'notification',
            audience: options.audience,
            notification: {
                title,
                body,
                big_text,
                channel_level: 3,
                click_type: 'payload',
                payload
            },
            push_channel: {
                ios: {},
                android: {}
            }
        }
        const { appId, appKey, appSecret, masterSecret } = accounts.appPush;

    }
}

module.exports = GeTui;
