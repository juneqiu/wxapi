// 公众号相关

'use strict';
const Service = require('egg').Service;
const crypto = require('crypto')
const wxBase = 'https://api.weixin.qq.com/cgi-bin'
const { createXml } = require('../utils/xml2js');

class Wechat extends Service {

    /**
     * verifySignature
     * @desc 验证消息是否来自微信服务器
     * {timestamp, nonce, token} 按字典排序组合在一起，拼接成字符串通过啥sha1加密对比发送过来的signature对比
     */
    async verifySignature(timestamp, nonce, signature) {
        const { app } = this
        const token = app.config.wechat.token
        const arr = [token, timestamp, nonce]
        const str = arr.sort().join('')
        const sha1Str = crypto.createHash('sha1').update(str, 'utf-8').digest('hex')
        if (signature !== sha1Str) return false
        return true
    }

    /**
     * getAccessToken
     * @desc 获取access_token 存redis
     */
    async getAccessToken() {
        const { ctx, app } = this
        const hasAccessToken = await ctx.service.redisService.get('accessToken')
        if (hasAccessToken) {
            return hasAccessToken
        } else {
            const getAccessToken = await app.curl(`${wxBase}/token?grant_type=client_credential&appid=${app.config.wechat.appId}&secret=${app.config.wechat.appSecret}`, {
                method: 'GET',
                dataType: 'json'
            })
            const token = getAccessToken.data.access_token
            // 7200秒过期，提前
            const hasAccessToken = await ctx.service.redisService.set('accessToken', token, 7180)
            return token
        }
    }

    /**
     * getTicket
     * @desc 获取ticket
     * @param { String } type ticket的type
     */
    async getTicket(type) {
        const { ctx, app } = this
        const hasTicket = await ctx.service.redisService.get(`ticket_${type}`)
        if (hasTicket) {
            return hasTicket
        } else {
            const accessToken = await this.getAccessToken()
            const ticketRes = await app.curl(`${wxBase}/ticket/getticket?access_token=${accessToken}&type=${type}`, {
                method: 'GET',
                dataType: 'json'
            })
            const ticket = ticketRes.data.ticket
            // 7200秒过期，提前
            const hasAccessToken = await ctx.service.redisService.set(`ticket_${type}`, ticket, 7180)
            return ticket
        }
    }

    /**
     * getUserInfo
     * @desc 获取用户信息
     * @param { String } openid 
     */
    async getUserInfo(openid) {
        const access_token = await this.getAccessToken()
        const res = await this.ctx.curl(`${wxBase}/user/info?access_token=${access_token}&openid=${openid}&lang=zh_CN`, {
            dataType: 'json',
        });
        return res.data;
    }
}

module.exports = Wechat;
