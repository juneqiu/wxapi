'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    require('./router/wxMini')(app);
    require('./router/wxFace')(app);
    require('./router/wechat')(app);
    require('./router/common')(app);
};