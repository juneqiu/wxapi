'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    const xml2js = app.middleware.xml2js()

    router.get('/', controller.wechat.wechat.home)
    router.post('/wechat', xml2js, controller.wechat.wechat.toWechat)
    router.get('/wechat', controller.wechat.wechat.verifyWechat)
    router.get('/getAccessToken', controller.wechat.wechat.getAccessToken)
};
