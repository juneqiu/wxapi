'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    router.get('/push/token', controller.common.push.getToken)
    router.get('/push/tosingle', controller.common.push.toSingle)
    router.get('/push/tolist', controller.common.push.toList)
    router.post('/push/toapp', controller.common.push.toApp)

    // cos
    router.post('/cos/upload', controller.common.cos.upload)
}