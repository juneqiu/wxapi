'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    router.get('/ocr', controller.faceTX.face.orcTest)
    router.get('/face', controller.faceTX.face.faceTest);
};
