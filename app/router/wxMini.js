'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    router.get('/openid', controller.wxMini.wxapi.getOpenId);
    router.get('/decrypt', controller.wxMini.wxapi.decrypt)
    router.get('/order', controller.wxMini.wxapi.createOrder)

    router.get('/app/order', controller.wxMini.wxapi.appcreateOrder)
};
