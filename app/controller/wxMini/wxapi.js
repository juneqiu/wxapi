'use strict';

const Controller = require('../../base/base_controller');
const utils = require('../../utils/wxpay')
const { createXml, parseXml } = require('../../utils/xml2js')
class WxApi extends Controller {

    // 获取openid
    async getOpenId() {
        const { ctx, app } = this
        const { code } = ctx.query
        const { appId, appSecret } = app.config.wechatMini
        let res = await app.curl(`https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=authorization_code`, {
            dataType: 'json'
        })
        this.success(res.data)
    }

    // 解密加密信息
    async decrypt() {
        const { ctx } = this
        const { encryptedData, iv, session_key } = ctx.query
        const res = await this.service.wxdecrypt.decrypt(session_key, encryptedData, iv)
        this.success(res)
    }


    //生成订单
    async createOrder() {
        const { ctx, app } = this
        const { openid } = ctx.query
        let params = {  // https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_1
            appid: app.config.wechatMini.appId,
            mch_id: app.config.wechatPay.MCHID,
            nonce_str: utils.rnd32(),
            sign_type: 'MD5',
            body: '支付测试',
            out_trade_no: utils.tradeNo(),
            total_fee: 1,
            spbill_create_ip: utils.ipAdress(),
            notify_url: '/pages/index/index',
            trade_type: 'JSAPI',
            openid
        }
        params.sign = utils.createSign(params, app.config.wechatPay.key)
        const paramsXml = createXml(params)
        let res = await app.curl(`https://api.mch.weixin.qq.com/pay/unifiedorder`, {
            method: 'POST',
            dataType: 'text',
            data: paramsXml
        })
        const jsonObj = await parseXml(res.data)
        const resData = {
            timeStamp: utils.timeStamp() + '',
            nonceStr: params.nonce_str,
            signType: 'MD5',
            package: `prepay_id=${jsonObj.prepay_id}`,
            appId: app.config.wechatMini.appId,
        }
        resData.paySign = utils.createSign(resData, app.config.wechatPay.key)
        this.success(resData)
    }

    // appcreateOrder
    async appcreateOrder() {
        const { ctx, app } = this
        let params = {
            appid: app.config.wetchatApp.appId,
            mch_id: app.config.wechatPay.MCHID,
            nonce_str: utils.rnd32(),
            sign_type: 'MD5',
            body: 'wechatapp支付测试',
            out_trade_no: utils.tradeNo(),
            notify_url: '/pages/index/index',
            spbill_create_ip: utils.ipAdress(),
            total_fee: 100,
            trade_type: 'APP'
        }
        params.sign = utils.createSign(params, app.config.wechatPay.key)
        const paramsXml = createXml(params)
        let res = await app.curl(`https://api.mch.weixin.qq.com/pay/unifiedorder`, {
            method: 'POST',
            dataType: 'text',
            data: paramsXml,
        })
        const jsonObj = await parseXml(res.data)
        const resData = {
            timestamp: utils.timeStamp() + '',
            noncestr: utils.rnd32(),
            package: 'Sign=WXPay',
            prepayid: jsonObj.prepay_id,
            partnerid: app.config.wechatPay.MCHID,
            appid: app.config.wetchatApp.appId,
        }
        resData.sign = utils.createSign(resData, app.config.wechatPay.key)

        this.success(resData)


    }
}

module.exports = WxApi;
