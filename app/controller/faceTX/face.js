'use strict';

const Controller = require('../../base/base_controller');
const utils = require('../../utils/wxface')
const { createXml, parseXml } = require('../../utils/xml2js')
const faceAuth = require('../../utils/wxFace')

class WxApi extends Controller {
    async orcTest() {
        try {
            const { ctx, app } = this
            let params = {
                version: '1.0.0',
                appId: 'TIDAhoTV',
                nonce: utils.rnd32(),
                userId: Math.floor(new Date().getTime()).toString(),
                orderNo: utils.tradeNo()
            }
            params.sign = utils.createSign(params)
            params.config = {
                "SDKType": 2
            }
            this.success(params)
        } catch (error) {
            this.fail(500, '服务器错误')
        }
    }

    async faceTest() {
        try {
            const { ctx, app } = this
            // const sign = await faceAuth.detectAuth()
            // console.log(sign)
            const test = {
                wbappid: 'IDAXXXXX',
                userId: 'userID19959248596551',
                nonceStr: 'kHoSxvLZGxSoFsjxlbzEoUzh5PAnTU7T',
                version: '1.0.0',
                ticket: 'XO99Qfxlti9iTVgHAjwvJdAZKN3nMuUhrsPdPlPVKlcyS50N6tlLnfuFBPIucaMS'
            }
            const res = await utils.createSign(test)
            console.log(res)
            this.success(res)
        } catch (error) {
            console.log(error)
            this.fail(500, '服务器错误')
        }
    }

    // 获取openid
    async getOpenId() {
        try {
            const { ctx, app } = this
            const { code } = ctx.query
            const { appId, appSecret } = app.config.wechatMini
            let res = await app.curl(`https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=authorization_code`, {
                dataType: 'json'
            })
            this.success(res.data)
        } catch (error) {
            this.fail(500, '服务器错误')
        }
    }

}

module.exports = WxApi;
