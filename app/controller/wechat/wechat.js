'use strict';

const Controller = require('../../base/base_controller');
const utils = require('../../utils/utils.js');
const { timeStamp } = require('../../utils/utils.js');

class WxApi extends Controller {
    /**
    * 微信授权登陆
    * https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx520c15f417810387&redirect_uri=https%3A%2F%2Fchong.qq.com%2Fphp%2Findex.php%3Fd%3D%26c%3DwxAdapter%26m%3DmobileDeal%26showwxpaytitle%3D1%26vb2ctag%3D4_2030_5_1194_60&response_type=code&scope=snsapi_base&state=123#wechat_redirect
    * https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf0e81c3bee622d60&redirect_uri=http%3A%2F%2Fnba.bluewebgame.com%2Foauth_response.php&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect
    */
    async wechatAuth() {

        const { ctx, app } = this
        const { appId, baseUrl } = app.config.wechat
        const redirectUrl = `${baseUrl}/home`
        const wxUrl = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appId}&redirect_uri=${encodeURIComponent(redirectUrl)}&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect`
        ctx.redirect(wxUrl)

    }

    // home
    async home() {

        const { ctx, app } = this
        const { appId, baseUrl } = app.config.wechat
        const ticket = await ctx.service.wechat.getTicket('jsapi')
        const { signature, nonceStr, timestamp } = utils.createSignature(ticket, `${baseUrl}/`)
        const data = {
            appId,
            timestamp,
            nonceStr,
            signature,
        }
        await this.ctx.render('home', data);

    }

    // 授权
    async oauth() {
        const access_token = res.data.access_token;
        const openid = res.data.openid;
        const userInfoUrl = config.getOauthUserInfoUrl.replace('ACCESS_TOKEN', access_token).replace('OPENID', openid);
        const userInfoRes = await this.ctx.curl(userInfoUrl, {
            dataType: 'json',
        });
        console.log(userInfoRes.data);
        this.ctx.body = 'hi, ' + userInfoRes.data.nickname;

    }

    // 微信消息
    async toWechat() {

        const { ctx, app } = this
        const message = ctx.req.body
        if (message) {
            const MsgType = message.MsgType;
            let reply;
            if (MsgType === 'event') {
                reply = await this.service.wechatMsg.handleEvent(message);
            } else {
                reply = await this.service.wechatMsg.handleMsg(message);
            }
            if (reply) {
                const result = await this.service.wechatMsg.replyMsg(message, reply);
                this.ctx.body = result;
                return true;
            }
        }
        ctx.body = 'success'

    }

    // 验证是否经过微信服务器
    async verifyWechat() {

        const { ctx, app } = this
        const { signature, echostr, timestamp, nonce } = ctx.query
        const verifyRes = await this.service.wechat.verifySignature(timestamp, nonce, signature)
        if (verifyRes) {
            // 这里不要返回其他任何东西过去，only echostr is 👌
            ctx.body = echostr
        } else {
            this.fail('error')
        }

    }

    // 获取access_token
    async getAccessToken() {

        const { ctx, app } = this
        const findAccessToken = await ctx.service.wechat.getAccessToken()
        this.success(findAccessToken)

    }

}

module.exports = WxApi;
