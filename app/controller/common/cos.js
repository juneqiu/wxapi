'use strict';

const Controller = require('egg').Controller;
const path = require('path');
const fs = require('mz/fs');
const accounts = require('../../../config/accounts.js');
const COS = require('cos-nodejs-sdk-v5');
const { resolve } = require('path');
const { rejects } = require('assert');
const { appId, secretId, secretKey } = accounts.tx;
const cos = new COS({
    AppId: appId,
    SecretId: secretId,
    SecretKey: secretKey,
});

class CosController extends Controller {
    async upload() {
        const { ctx, app } = this;
        if (!ctx.request.files) return ctx.body = '请先选择上传文件';
        const file = ctx.request.files[0];
        const { bucket, region } = accounts.tx_cos;
        const filename = 'tx-cos' + Date.now() + path.extname(file.filename); // path.extname 返回path的扩展名
        const params = {
            Bucket: bucket,
            Region: region,
            Key: filename,
            FilePath: file.filepath
        }
        const handleUpload = (() => {
            return new Promise((resolve, reject) => {
                cos.sliceUploadFile(params, (err, data) => {
                    if (err) {
                        fs.unlinkSync(file.filepath); // 地删除文件或符号链接
                        reject({
                            cos_status: false,
                            msg: err
                        })
                    }
                    const imageSrc = 'https://june-1300185806.cos.ap-nanjing.myqcloud.com/' + data.Key;
                    resolve({
                        cos_status: true,
                        msg: imageSrc
                    })
                });
            });
        })
        const result = await handleUpload()
        if (!result.cos_status) {
            ctx.status = 400;
            return ctx.body = '上传错误';
        }
        ctx.status = 200;
        return ctx.body = result

    }
}

module.exports = CosController;
