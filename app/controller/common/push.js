'use strict';

const Controller = require('egg').Controller;
const { formatPushMsg } = require('../../utils/push.js')

const crypto = require('crypto');
const baseUrl = 'https://restapi.getui.com/v2/';
const accounts = require('../../../config/accounts.js');

class PushController extends Controller {
    // 获取个推的token
    async getToken() {
        const { ctx, service } = this;
        const res = await service.push.getGeTuiToken()
        await service.cache.set('getui_token', res.token, parseInt(res.expire_time) + 86000000);
        ctx.body = res;
    }

    // 单推
    async toSingle() {
        const { ctx, app, service } = this;
        const { title, body, big_text, payload, cid } = ctx.request.body;
        let token = await service.cache.get('getui_token');
        if (!token) {
            token = await service.push.getGeTuiToken();
        }
        const options = {
            type: 'notification',
            audience: { // 推送目标用户
                cid: ['22']
            },
            notification: {
                title,
                body,
                big_text,
                channel_level: 3,
                click_type: 'payload',
                payload
            },
            push_channel: {
                ios: {},
                android: {}
            }
        }
        const formatMsg = await formatPushMsg(options)
        ctx.body = {
            token,
            formatMsg
        }
    }

    // 批量推
    async toList() {
        const { ctx, app, service } = this;
    }

    // 群推
    async toApp() {
        const { ctx, app, service } = this;
        const { appId } = accounts.appPush;
        let token = await service.cache.get('getui_token');
        if (!token) {
            token = await service.push.getGeTuiToken();
        }
        const { title, body_msg, big_text, click_type, payload } = ctx.request.body;
        console.log(title, body_msg, big_text, click_type, payload)
        if (!title || !body_msg || !click_type) return ctx.body = '参数不正确';

        const ress = await ctx.curl(`${baseUrl}${appId}/push/all`, {
            method: 'POST',
            contentType: 'json',
            dataType: 'json',
            data: {
                request_id: 'june' + Date.now() + 'push',
                audience: 'all',
                push_message: {
                    notification: {
                        title,
                        body: body_msg,
                        big_text,
                        channel_level: 3,
                        click_type: 'payload',
                        payload
                    }
                },
                push_channel: {
                    android: {
                        ups: {
                            notification: {
                                title,
                                body: body_msg,
                                click_type: 'payload',
                                payload
                            }
                        }
                    }
                }
            },
            headers: {
                token
            }
        });
        ctx.body = ress
    }
}

module.exports = PushController;
