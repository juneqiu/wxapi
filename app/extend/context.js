'use strict';

module.exports = {
    SUCCESS_CODE: 200, // 成功
    UNLOGIN_CODE: 1001, // 未登录
    ERROR_CODE: 500, // 系统错误
};
